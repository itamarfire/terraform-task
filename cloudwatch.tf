
resource "aws_iam_role" "cloudwatch_events_role" {
  name = "task-events"
  assume_role_policy = data.aws_iam_policy_document.events_assume_role_policy.json
}

data "aws_iam_policy_document" "events_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}
resource "aws_cloudwatch_event_rule" "scheduled_task" {
  name = "scheduled_task"
  description = "Runs fargate task scheduled_task: ${var.schedule_expression}"
  schedule_expression = var.schedule_expression
}

resource "aws_cloudwatch_event_target" "scheduled_task" {
  rule = aws_cloudwatch_event_rule.scheduled_task.name
  target_id = aws_cloudwatch_event_rule.scheduled_task.name
  arn = aws_ecs_cluster.main.arn
  role_arn = aws_iam_role.cloudwatch_events_role.arn
  input = "{}"

  ecs_target {
    task_count = 1
    task_definition_arn = aws_ecs_task_definition.app.arn
    launch_type = "FARGATE"
    platform_version = "LATEST"

    network_configuration {
      assign_public_ip = false
      security_groups = [aws_security_group.ecs_tasks.id]
      subnets = "${aws_subnet.private.*.id}"
    }
  }
}
