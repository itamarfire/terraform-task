resource "google_project_service" "project" {
  project = google_project.default.project_id
  service = "firebase.googleapis.com"

  disable_dependent_services = true
}

resource "google_firebase_project" "default" {
  provider = google-beta
  project  = google_project.default.project_id
}

resource "google_project" "default" {
    provider = google-beta

    project_id = "terraform-task2000"
    name       = "terraform-task2000"
    org_id     = "123456789"
}
