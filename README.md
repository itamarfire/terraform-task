# terraform-task

## Firebase
1. use the following guide to create a Terraform Admin Project:
https://cloud.google.com/community/tutorials/managing-gcp-projects-with-terraform
2. use the following link to create service account key:
https://console.cloud.google.com/apis/credentials/serviceaccountkey
3. set up the following enviroment variable to the service account key location, for example:
export GOOGLE_APPLICATION_CREDENTIALS="/home/itamar/temp/terraform-task-AK.json"
4. cd to firebase directory and execute "terraform apply"

## AWS
1. export AWS admin credentials (AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,AWS_DEFAULT_REGION)
2. execute "terraform apply", to check it out enter the DNS output address in the browser ;)
